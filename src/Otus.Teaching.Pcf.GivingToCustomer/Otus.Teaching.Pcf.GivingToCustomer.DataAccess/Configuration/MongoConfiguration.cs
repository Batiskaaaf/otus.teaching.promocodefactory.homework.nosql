namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Configuration
{

    public class MongoConfiguration
    {
        public string ConnectionString { get; set; }       
        public string Database { get; set; }       
    }
}