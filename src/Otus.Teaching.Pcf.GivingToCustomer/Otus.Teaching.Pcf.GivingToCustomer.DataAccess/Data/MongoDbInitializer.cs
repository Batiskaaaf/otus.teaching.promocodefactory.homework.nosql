using System.Linq;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IRepository<Preference> preferenceRepository;
        private readonly IRepository<Customer> customerRepository;

        public MongoDbInitializer(
            IRepository<Preference> preferenceRepository, 
            IRepository<Customer> customerRepository)
        {
            this.preferenceRepository = preferenceRepository;
            this.customerRepository = customerRepository;
        }

        public async void InitializeDb()
        {
            var preferences = await preferenceRepository.GetAllAsync();
            if(preferences.Any())
                return;

            foreach (var preference in FakeDataFactory.Preferences)
            {
                await preferenceRepository.AddAsync(preference);
            }
            foreach (var customer in FakeDataFactory.Customers)
            {
                await customerRepository.AddAsync(customer);
            }     
        }
    }
}
